# lidarHD 1.0.0

## Ajout

* `compute_dtm()` permet de calculer des modèles numériques de terrain à partir d'une sélection de dalles LiDAR.
* `add_attributes_ta()` ajoute dans le tableau d'assemblage le chemin d'accès aux fichiers LiDAR HD déjà téléchargés et aux produits dérivés (nuages normalisés, modèle de terrain) calculés à partir de ceux-ci.
* tutoriel mis à jour avec le calcul de modèles numériques de terrain.

## Changement

* Les dialogues utilisateur ont été traduits en français.
* `load_classified_ta()` a un argument `save_dir` pour sauvegarder le tableau sur disque ou charger une sauvegarde existante.
* `create_lax()` utilise désormais la fonction `rlas::writelax` par défaut.

