# _lidarHD_
<!-- badges: start -->
[![Lifecycle: stable](https://img.shields.io/badge/lifecycle-stable-brightgreen.svg)](https://lifecycle.r-lib.org/articles/stages.html#stable)
<!-- badges: end -->

# Un package R pour télécharger et gérer les fichiers lidar HD

`lidarHD` propose des outils pour télécharger et gérer les fichiers de la couverture nationale [LIDAR HD](https://geoservices.ign.fr/lidarhd). L'IGN met en ligne les données sous forme de dalles groupées par "blocs". Les fonctions du package `lidarHD` servent à :

* télécharger les fichiers des dalles qui intersectent une zone d'intérêt et les enregistrer par bloc ;
* créer des catalogues des fichiers téléchargés sous forme d'objets au format `LAS-catalog`, afin de faciliter les traitements ultérieurs ;
* normaliser les fichiers pour remplacer l'altitude par la hauteur par rapport au sol ;
* calculer des modèles numériques de terrain.

# Installation

* `R` >= 4.2.3 recommandé, packages `lidR` >= 4.1.1 et `rlas` >= 1.7 requis.
* Installer le package [`devtools`](https://cran.r-project.org/package=devtools) puis exécuter dans une console `R` :  `devtools::install_git("https://forgemia.inra.fr/lidar/lidarHD")`.

# Tutoriel

Un [tutoriel de prise en main](https://lidar.pages.mia.inra.fr/lidarHD/articles/lidarHD.html) au format `Rmarkdown` est disponible dans le répertoire [vignettes](https://forgemia.inra.fr/lidar/lidarHD/-/tree/main/vignettes).