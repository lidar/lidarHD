# package lidarHD
# Copyright INRAE
# Author(s): Jean-Matthieu Monnet
# Licence: GPL-3
# ------------------------- FUNCTIONS FOR PROCESSING FILES -----------------------
#
## Normalisation des fichiers LiDAR HD  ----
#' Normalisation des fichiers LiDAR HD 
#' 
#' Normalise les fichiers LiDAR HD de l'IGN presents dans un dossier (par exemple 
#' telecharges avec \code{\link{download_files}}) et les enregistre dans un 
#' autre repertoire. Les fichiers normalises deja presents ne sont pas re-calcules. La 
#' normalisation est effectuee avec la fonction \code{\link[lidR]{normalize_height}}
#' et l'algorithme \code{\link[lidR]{tin}}. Les fichiers a normaliser peuvent etre
#' selectionnes via une zone d'interet, il est conseille que les fichiers adjacents
#' soient egalement telecharges pour eviter les effets de bord.
#' 
#' @param original Text, path to the folder that contains the laz files downloaded 
#' from IGN. The folder should contain a "cata.rda" file with the saved 
#' \code{\link[lidR]{LAScatalog-class}} object of files in the folder. 
#' @param normalized Text, path to the folder that contains or will contain the 
#' normalized laz files. If some files are already present, the folder should contain
#' a "cata.rda" file with the saved \code{\link[lidR]{LAScatalog-class}} object
#' of files in the folder. 
#' @param roi \code{sf} object or character vector. Region of interest if 
#' \code{sf} object, "blocs" of interest if character vector.
#' @param prompt Boolean. Should the user be prompted before plotting and 
#' normalizing ?  
#' @param ncores Integer, number of cores to use with \code{future} package.
#' @return The updated \code{\link[lidR]{LAScatalog-class}} object of normalized
#' files in the \code{normalized} folder.
#' @examples
#' \dontrun{normalize_files("./data/LAZ_classe/", "./data/LAZ_norm/", prompt = TRUE, ncores = 2L)}
#' @export
normalize_files <- function(original, normalized, roi = NULL, prompt = TRUE, ncores = 2L)
{
  # load catalog of normalized files
  if (file.exists(paste0(normalized, "/cata.rda")))
  {
    load(paste0(normalized, "/cata.rda"))
  } else {
    cata <- NULL
  }
  cata_norm <- cata
  #
  # load catalog of original files
  load(paste0(original, "cata.rda"))
  # 
  # flag tiles to process
  if (!is.null(roi))
  {
    cata <- flag_processed(cata, roi, keep_all = FALSE)
  } else {
    cata$processed <- TRUE
  }
  # create vector of action status
  action <- ifelse(cata$processed, "a_faire", "hors_zone")
  # flag tiles already processed
  if (!is.null(cata_norm))
  {
    dummy <- which(is.element(gsub("copc.laz$", "copc.norm.laz", basename(cata$filename)), basename(cata_norm$filename)))
    cata$processed[dummy] <- FALSE
    action[dummy] <- paste0(action[dummy], "_fait")
    action <- gsub("a_faire_fait", "fait", action)
  }
  # user interaction
  if (prompt)
  {
    # print table of number of files to process by bloc
    print(table(action, bloc = cata$bloc))
    dummy <- as.character(readline(prompt = "Afficher la carte map ? (o / n)"))
  } else {
    dummy <- "n"
  }
  if (dummy == "o")
  {
    plot(sf::st_geometry(cata@data), col = as.factor(action))
    if (inherits(roi, "sfc")) plot(sf::st_geometry(roi), border = "green",
                                   add = TRUE, col = NA)
  }
  #
  indices_to_process <- which(cata$processed)
  # 
  if (length(indices_to_process) < 1)
  {
    message("Aucun fichier a normaliser")
    return(cata)
  }
  #
  if (prompt)
  {
    dummy <- as.character(readline(prompt = "Normaliser les fichiers ? (o / n)"))
  } else {
    dummy <- "o"
  }
  #
  if (dummy == "n") return(cata)
  #
  # Catalog options 
  # laz compression
  lidR::opt_laz_compression(cata) <- TRUE
  # buffer for processing: use default of lidR
  # lidR::opt_chunk_buffer(cata) <- 5
  # process by file
  lidR::opt_chunk_size(cata) <- 0
  # display progress on map
  lidR::opt_progress(cata) <- FALSE
  # carry on even if one chunk returns error
  lidR::opt_stop_early(cata) <- FALSE
  #
  # bloc with change
  changed_blocs <- unique(cata$bloc[which(cata$processed == TRUE)])
  # create new files by bloc
  # because bloc info is require in output path of catalog engine
  for (bloc in rev(changed_blocs))
  {
    print(bloc)
    # set temporary catalog
    cata_temp <- cata
    # only files from this bloc are processed
    cata_temp$processed[cata_temp$bloc != bloc] <- FALSE
    # create folder if not present
    if(!dir.exists(paste0(normalized, "/", bloc))) dir.create(paste0(normalized, "/", bloc))
    # output files path
    lidR::opt_output_files(cata_temp) <- paste0(normalized, "/", bloc, "/{ORIGINALFILENAME}.norm")
    # set cluster
    future::plan(future::multisession, workers = ncores)
    # process with catalog engine
    output <- lidR::normalize_height(cata_temp, lidR::tin())
    # create lax index files
    dummy <- lidarHD::create_lax(paste0(normalized, "/", bloc), proceed = TRUE)
    # update bloc catalog
    dummy <- dir(paste0(normalized, bloc), pattern = "laz$")
    if (length(dummy) > 0) lidarHD::catalog_bloc(normalized, bloc)
  }
  # stop multissession
  future::plan(NULL)
  # update global catalog
  lidarHD::catalog_folder(normalized, changed_blocs)
}
#
## Calcul de MNT a partir des fichiers LiDAR HD  ----
#' Calcul de MNT a partir des fichiers LiDAR HD 
#' 
#' Calcul des fichiers MNT a partir des fichiers LiDAR HD de l'IGN presents dans
#'  un dossier (par exemple  telecharges avec \code{\link{download_files}}) et 
#' les enregistre dans un autre repertoire. Les fichiers MNT deja presents ne 
#' sont pas re-calcules. Le calcul est effectuee avec la fonction 
#' \code{\link[lidR]{rasterize_terrain}} et l'algorithme \code{\link[lidR]{tin}}
#' avec les points de classe 2 et 9. Les fichiers a traiter peuvent etre
#' selectionnes via une zone d'interet, il est conseille que les fichiers adjacents
#' soient egalement telecharges pour eviter les effets de bord.
#' 
#' @param laz_folder Text, path to the folder that contains the laz files downloaded 
#' from IGN. The folder should contain a "cata.rda" file with the saved 
#' \code{\link[lidR]{LAScatalog-class}} object of files in the folder. 
#' @param dtm_folder Text, path to the folder that contains or will contain the 
#' DTM files. If NULL a folder is created in the same directory as `laz_folder` 
#' with name "DTM_#res#m" where #res# is parameter `res`.
#' @param res numeric, DTM resolution 
#' @param roi \code{sf} object or character vector. Region of interest if 
#' \code{sf} object, "blocs" of interest if character vector.
#' @param prompt boolean. Should the user be prompted before plotting and 
#' computing ?  
#' @param ncores Integer, number of cores to use with \code{future} package.
#' @return A data.frame with the files to compute the DTM from and a boolean indicating if the computation was successful or not.
#' @examples
#' \dontrun{compute_dtm("./data/LAZ_classe/", res = 1, prompt = TRUE, ncores = 2L)}
#' @export
compute_dtm <- function(laz_folder, dtm_folder = NULL, res = 1, roi = NULL, prompt = TRUE, ncores = 2L)
{
  # load catalog of files
  if (file.exists(paste0(laz_folder, "cata.rda")))
  {
    load(paste0(laz_folder, "cata.rda"))
  } else {
    message("Missing cata.rda file in laz_folder")
    return(NULL)
  }
  # output directory
  if(is.null(dtm_folder)) dtm_folder <- paste0(dirname(laz_folder), "/DTM_", res, "m/")
  # 
  # flag tiles to process
  cata <- flag_processed(cata, roi, keep_all = FALSE)
  # create vector of action status
  action <- ifelse(cata$processed, "a_faire", "hors_zone")
  # check already processed file son disk
  to_process <- which(cata$processed == TRUE)
  # files to process (gsub(EB) pour cas donnees Suisse)
  fichiers <- gsub("/EB-", "/", cata$filename[to_process])
  # target file names
  fichiers <- laz_name2tif_name(fichiers, 1, dtm_folder)
  # handle existing files
  dummy <- file.exists(fichiers)
  # files to check after processing
  fichiers_to_check <- fichiers[!dummy]
  # remove from process
  cata$processed[to_process][dummy] <- FALSE
  # table(cata$processed, cata$bloc)
  action[to_process][dummy] <- paste0(action[to_process][dummy], "_fait")
  action <- gsub("a_faire_fait", "fait", action)
  # user interaction
  if (prompt)
  {
    # print table of number of files to process by bloc
    print(table(action, bloc = cata$bloc))
    dummy <- as.character(readline(prompt = "Afficher la carte map ? (o / n)"))
  } else {
    dummy <- "n"
  }
  if (dummy == "o")
  {
    plot(sf::st_geometry(cata@data), col = as.factor(action))
    if (inherits(roi, "sfc")) plot(sf::st_geometry(roi), border = "green",
                                   add = TRUE, col = NA)
  }
  #
  indices_to_process <- which(cata$processed)
  # 
  if (length(indices_to_process) < 1)
  {
    message("No files to compute")
    return(cata)
  }
  #
  if (prompt)
  {
    dummy <- as.character(readline(prompt = "Calculer les MNT ? (o / n)"))
  } else {
    dummy <- "o"
  }
  #
  if (dummy == "n") return(cata)
  #
  # create output directory if missing
  if(!dir.exists(dtm_folder)) dir.create(dtm_folder)
  # Catalog options 
  # process by file
  lidR::opt_chunk_size(cata) <- 0
  # buffer size for DTM computation and normalization
  # lidR::opt_chunk_buffer(cata) <- 10
  lidR::opt_filter(cata) <- "-keep_class 2 9"
  lidR::opt_progress(cata) <- FALSE
  # carry on even if one chunk returns error
  lidR::opt_stop_early(cata) <- TRUE
  # bloc with change
  changed_blocs <- unique(cata$bloc[which(cata$processed == TRUE)])
  # create new files by bloc
  # because bloc info is require in output path of catalog engine
  for (bloc in rev(changed_blocs))
  {
    if(prompt) print(bloc)
    # create folder if not present
    if (!dir.exists(paste0(dtm_folder, bloc))) dir.create(paste0(dtm_folder, bloc))
    # set temporary catalog
    cata_temp <- cata
    # only files from this bloc are processed
    cata_temp$processed[cata_temp$bloc != bloc] <- FALSE
    # output files path
    lidR::opt_output_files(cata_temp) <- paste0(dtm_folder, bloc, "/dtm", res, "m_{ORIGINALFILENAME}")
    # lidR::opt_output_files(cata_temp) <- paste0("~/dtm", res, "m_{ORIGINALFILENAME}")
    # set cluster
    future::plan(future::multisession, workers = ncores)
    # process with catalog engine
    dtm <- lidR::rasterize_terrain(cata_temp, res, lidR::tin())
  }
  # stop multissession
  future::plan(NULL)
  # check files
  if(prompt) print("Verification des fichiers calcules")
  future::plan(future::multisession, workers = ncores)
  dummy <- future.apply::future_lapply(fichiers_to_check, function(x) try(terra::global(terra::rast(x), mean), silent = TRUE), future.seed = TRUE)
  dummy <- sapply(dummy, function(x) !inherits(x, "try-error"))
  dummy <- data.frame(fichiers = fichiers_to_check, computed = dummy)
  # stop multissession
  future::plan(NULL)
  #
  if(sum(!dummy$computed)>0)
  {
    warning("Certains fichiers non calcules ou corrompus")
  }
  dummy
}
## Add Height above ground as extrabyte attribute ----
#' Add Height above ground as extrabyte attribute 
#' 
#' Adds the height above ground in an ExtraByte attribute \code{H} to objects of
#' class \code{\link[lidR]{LAS-class}} or to files recorded in an object of 
#' class \code{\link[lidR]{LAScatalog-class}}, by applying the function 
#' \code{\link[lidR]{normalize_height}}
#' 
#' @param las An object of class \code{\link[lidR]{LAS-class}} or 
#' \code{\link[lidR]{LAScatalog-class}}
#' @return A \code{\link[lidR]{LAS-class}} object with the \code{H} attribute in the 
#' case. In case of a \code{\link[lidR]{LAScatalog-class}} input, the function 
#' is used for the side effect of writing files as specified in the catalog
#' options.
#' @examples
#' \dontrun{add_height_extrabyte}
#' @export
add_height_extrabyte <- function(las)
{
  # if catalog, apply function to catalog
  if (methods::is(las, "LAScatalog")) {
    # ensure no attribute is lost
    lidR::opt_select(las) <- "*" # disable select tuning
    options <- list(automerge = FALSE, need_output_file = TRUE, need_buffer = TRUE)
    output <- lidR::catalog_apply(las, add_height_extrabyte)
    return(output)
    # if catalog chunk, apply function to points then remove buffer
  } else if (methods::is(las, "LAScluster")) {
    x <- lidR::readLAS(las)
    if (lidR::is.empty(x)) return(NULL)
    x <- add_height_extrabyte(x)
    buffer <- NULL # added just to avoid NOTE while checking package
    # otherwise buffer in next line is considered global variable
    x <- lidR::filter_poi(x, buffer == 0)
    return(x)
    # if las, apply function
  } else if (methods::is(las, "LAS")) {
    if (lidR::is.empty(las)) return(NULL)
    # normalize
    las <- lidR::normalize_height(las, lidR::tin()) # normalize
    # put height into extrabyte H field
    las <- lidR::add_lasattribute_manual(las, las$Z, name = "H",
                                       desc = "Height above ground",
                                       type = "int", offset = 0, scale = 0.001)
    # put back elevation in Z field
    las <- lidR::unnormalize_height(las)
    return(las) # output
  } else {
    stop("Not supported input")
  }
}
#
## Add Height Above Ground to a selection of files in a catalog ----
#' Add Height Above Ground to a selection of files in a catalog 
#' 
#' Adds the height above ground in an ExtraByte attribute \code{H} to files of a 
#' catalog. Files are renamed with the prefix "EB-" and are indexed with a 
#' ".lax" file because they are not written as "copc" files. Applies the function 
#' \code{\link{add_height_extrabyte}}, and requires that adjacent tiles are 
#' present in the catalog to avoid border effects.
#' 
#' @param cata A \code{\link[lidR]{LAScatalog-class}} object. In case no region 
#' of interest is provided and the catalog object has a "processed" attribute, 
#' only files with \code{TRUE} values are processed.
#' @param roi \code{sf} object or character vector. Region of interest if 
#' \code{sf} object, "blocs" of interest if character vector.
#' @param prompt boolean. Should the user be prompted before plotting and 
#' normalizing ?  
#' @param ncores Integer, number of cores to use with \code{future} package.
#' @return The updated \code{\link[lidR]{LAScatalog-class}} object.
#' @examples
#' \dontrun{add_height_catalog}
#' @export
add_height_catalog <- function(cata, roi, prompt = TRUE, ncores = 2)
{
  # folder with blocs directories and files
  dossier <- dirname(dirname(cata$filename[1]))
  # identify files which already have H in EB attribute
  cata$EB <- substr(basename(cata$filename), 1, 3) == "EB-"
  #
  if(is.null(roi))
  {
    if(!is.element("processed", names(cata@data)))
    {
      cata$processed <- TRUE
      warning("All catalog is processed, 
              make sure that there are no border effects")
    }
  } else {
    cata$processed <- FALSE
    if(is.element("character", class(roi)))
    {
      # files from whole bloc(s) to process
      cata$processed[which(is.element(cata$bloc, roi))] <- TRUE
    } else {
      roi <- sf::st_union(sf::st_geometry(roi))
      sf::st_crs(roi) <- sf::st_crs(cata@data)
      # files in roi to process
      cata$processed[which(sf::st_intersects(cata@data, roi, sparse = FALSE))] <- TRUE
    }
  }
  # keep only tiles relevant for processing in catalog
  # tiles to process
  dummy <- sf::st_union(sf::st_geometry(cata@data[cata$processed == TRUE,]))
  # add 100 m buffer
  dummy <- sf::st_buffer(dummy, 100)
  # select tiles to process and adjacent ones in catalog
  cata <- cata[which(sf::st_intersects(cata@data, dummy, sparse = FALSE)), ]
  # 
  print(table(processed = cata$processed, bloc = cata$bloc))
  #
  if (prompt)
  {
    dummy <- as.character(readline(prompt = "Display map ? (y / n)"))
  } else {
    dummy <- "n"
  }
  if (dummy == "y") plot(cata@data['processed'], border = NA)
  #
  indices_to_process <- which(cata$processed)
  # 
  if (length(indices_to_process) < 1)
  {
    message("No files to normalize")
    return(cata)
  }
  #
  if (prompt)
  {
    dummy <- as.character(readline(prompt = "Normalize files ? (y / n)"))
  } else {
    dummy <- "y"
  }
  #
  if (dummy == "n") return(cata)
  #
  ## Catalog options
  # laz compression
  lidR::opt_laz_compression(cata) <- TRUE
  # buffer for processing
  # lidR::opt_chunk_buffer(cata) <- 5
  # process by file
  lidR::opt_chunk_size(cata) <- 0
  # display progress on map
  # lidR::opt_progress(cata) <- TRUE
  # carry on even if one chunk returns error
  lidR::opt_stop_early(cata) <- FALSE
  #
  # initialize processing by bloc
  changed_blocs <- unique(cata$bloc[which(cata$processed == TRUE)])
  # list to hold original files to delete
  files_to_delete <- list()
  # create new files by bloc
  for (bloc in changed_blocs)
  {
    print(paste0("Bloc ", bloc))
    # create copy of cata
    cata_temp <- cata
    # with only files on one bloc to process
    cata_temp$processed[cata_temp$bloc != bloc] <- FALSE
    # set names for output file
    lidR::opt_output_files(cata_temp) <- 
      paste0(dossier, "/", bloc, "/EB-{ORIGINALFILENAME}")
    # set parallel processing
    future::plan(future::multisession, workers = ncores)
    # apply lidarHD function for normalizing files in a catalog
    print("Normalisation")
    output <- lidarHD::add_height_extrabyte(cata_temp)
    # output EB files (start with "EB-", ends with "laz")
    files_eb <- dir(paste0(dossier, "/", bloc),
                    pattern = "^EB-.*.laz$", full.names = TRUE)
    # original LAZ to delete
    files_to_delete[[bloc]] <- gsub("EB-", "", files_eb)
    # rename output EB files to remove "copc" as files are not indexed
    dummy <- file.rename(files_eb, gsub("copc.laz$", "laz", files_eb))
    # index files
    print("Create lax index of new files")
    dummy <- lidarHD::create_lax(paste0(dossier, "/", bloc), proceed = TRUE)
  }
  # stop multissession
  future::plan(NULL)
  # remove original files
  files_to_delete <- unlist(files_to_delete)
  all(file.exists(files_to_delete))
  unlink(files_to_delete)
  # update blocs catalog
  print("Update bloc catalogs")
  for (bloc in changed_blocs)
  {
    print(bloc)
    lidarHD::catalog_bloc(dossier, bloc)
  }
  # update global catalog
  print("Update global catalog")
  lidarHD::catalog_folder(dossier, changed_blocs)
}